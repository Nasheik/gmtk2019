﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //[Header("Settings")]

    //[Header("References")]

    //[Header("Prefabs")]

    //[Header("Accessors")]

    //[Header("Private")]


    void Awake()
    {
        
    }

    void Start()
    {
        
    }

    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name != "Player")
        {
            return;
        }

        Life enemyLife = GetComponent<Life>();
        Rigidbody2D enemyRb2d = GetComponent<Rigidbody2D>();
        Life collisionLife = collision.gameObject.GetComponent<Life>();
        Rigidbody2D collisionRb2d = collision.gameObject.GetComponent<Rigidbody2D>();


        float collisionVelocity = collisionRb2d.velocity.magnitude;
        float enemyVelocity = enemyRb2d.velocity.magnitude;

        if (enemyVelocity <= collisionVelocity)
        {
            if (collisionVelocity < enemyLife.GetHealth())
            {
                collisionLife.Damage((int)collisionVelocity);
            }

            enemyLife.Damage((int)collisionVelocity);            
        }
        else
        {
            if (enemyVelocity < collisionLife.GetHealth())
            {
                enemyLife.Damage((int)enemyVelocity);
            }

            collisionLife.Damage((int)enemyVelocity);
        }
    }
}
