﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    [Header("Settings")]
    public float playerToMoveThreshold;

    //[Header("References")]

    //[Header("Prefabs")]

    //[Header("Accessors")]

    [Header("Private")]
    InputController inputController;

    void Awake()
    {

    }

    void Start()
    {
        inputController = GetComponent<InputController>();
    }

    void Update()
    {
        Vector3 playerToMouse = inputController.currentMouseWorldPosition - transform.position;
        if (playerToMouse.sqrMagnitude > playerToMoveThreshold)
        {
            transform.up = playerToMouse;
        }
    }
}
