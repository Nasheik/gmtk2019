﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Settings")]
    public float playerToMoveThreshold;
    public float movementForceMultiplier;

    //[Header("References")]

    //[Header("Prefabs")]

    //[Header("Accessors")]

    //[Header("Private")]
    InputController inputController;
    Rigidbody2D rb2d;


    void Awake()
    {

    }

    void Start()
    {
        inputController = GetComponent<InputController>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Vector3 playerToMouse = inputController.currentMouseWorldPosition - transform.position;
        //print(playerToMouse + "   " + playerToMouse.sqrMagnitude);
        if (playerToMouse.sqrMagnitude > playerToMoveThreshold)
        {
            rb2d.AddForce(playerToMouse * movementForceMultiplier);
        }
    }
}
