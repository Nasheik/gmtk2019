﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [Header("Accessors")]
    public Vector3 currentMouseScreenPosition;
    public Vector3 currentMouseWorldPosition;

    [Header("Private")]
    Camera mainCamera;


    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        currentMouseScreenPosition = Input.mousePosition;
        currentMouseWorldPosition = mainCamera.ScreenToWorldPoint(currentMouseScreenPosition);
        currentMouseWorldPosition.z = 0;
    }
}
