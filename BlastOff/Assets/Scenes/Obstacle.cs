﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Life))]
public class Obstacle : MonoBehaviour
{
    //[Header("Settings")]

    //[Header("References")]

    //[Header("Prefabs")]

    //[Header("Accessors")]

    //[Header("Private")]


    void OnCollisionEnter2D(Collision2D collision)
    {
        Life obstacleLife = GetComponent<Life>();
        Life collisionLife = collision.gameObject.GetComponent<Life>();

        float collisionVelocity = collision.relativeVelocity.magnitude;

        obstacleLife.Damage((int)collisionVelocity);

        if (collisionVelocity < obstacleLife.GetHealth())
        {
            collisionLife.Damage((int)collisionVelocity);
        }
    }
}
