﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Life : MonoBehaviour
{
    [Header("Settings")]
    public int maxHealth;

    [Header("References")]
    public Text healthText;

    //[Header("Prefabs")]

    //[Header("Accessors")]

    [Header("Private")]
    int health;


    void Awake()
    {
        health = maxHealth;
        if (healthText != null)
        {
            healthText.text = health.ToString();
        }
    }

    public int GetHealth()
    {
        return health;
    }

    public void Damage(int dmg)
    {
        health -= dmg;
        if(health <= 0)
        {
            health = 0;
            Death();
        }
        if (healthText != null)
        {
            healthText.text = health.ToString(); 
        }
    }

    void Death()
    {
        Destroy(gameObject);
    }
}
