﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLocationManager : MonoBehaviour
{
    public static CameraLocationManager instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void MoveCamera(Vector3 position)
    {
        position.z = -10;
        transform.position = position;
    }
}
