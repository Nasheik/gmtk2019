﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCollider : MonoBehaviour
{
    [Header("Settings")]
    public Transform room;


    void OnTriggerEnter2D(Collider2D collision)
    {
        print(collision.gameObject.name);
        if(collision.gameObject.name != "Player")
        {
            return;
        }
        print("hiee");
        CameraLocationManager.instance.MoveCamera(room.position);
    }
}
